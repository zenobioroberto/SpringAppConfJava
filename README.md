# Spring Application Config via Java

Progetto associato al corso **Spring for Beginner**

Lo scopo è quello di mostrare un progetto Legacy: aka un progetto configurato senza spring boot, via XML.
In questo progetto mostreremo:

* Configurazioen maven
* Bootstrap dell'applicazione jar
* Creazione dei primi bean
* Factory Bean
* Application Contex 

Questo progetto fa riferimento alla prima lezione del corso