package it.nesea.springappconfjava.conf;

import it.nesea.springappconfjava.beans.User;
import it.nesea.springappconfjava.factory.Printable;
import it.nesea.springappconfjava.factory.PrintableFactory;
import it.nesea.springappconfjava.factory.Shop;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("it.nesea.springappconfjava.services")
public class AppConf {

    /**
     * Inserisce il nostro Singleton all'interno del container.
     * @return
     */
    @Bean(name = "shop")
    public Shop shopBean(){
        return Shop.getInstance();
    }

    /**
     * Crea un nuovo ben marcato come moderator e con dei valori di inizializzazione
     * @return
     */
    @Bean(name = "moderator")
    public User moderatorBean(){
        return new User(2,"moderator", "moderator");
    }

    /**
     * Inseriamo all'interno del container una factory di bean
     * @return
     */
    @Bean(name = "pfactory")
    public PrintableFactory pfactoryBean(){
        return new PrintableFactory();
    }

    /**
     * Andiamo ad utilizzare la factory, presente all'interno del container
     * per generare un nuovo bean (Non static) da inserire con il nome di printableNS
     * @return
     */
    @Bean(name = "printableNS")
    public Printable printableNSBean(){
        return pfactoryBean().getPrintableNonStatic();
    }

}
