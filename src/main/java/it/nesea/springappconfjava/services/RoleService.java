package it.nesea.springappconfjava.services;

import it.nesea.springappconfjava.beans.Role;
import it.nesea.springappconfjava.beans.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {
    @Autowired
    User moderator;

    public void assignStandardRoleToModerator() {
        Role role = new Role("MODERATOR", "MODERATOR");
        moderator.getRoles().add(role);
    }
}
