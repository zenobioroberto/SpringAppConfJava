package it.nesea.springappconfjava.factory;

import org.springframework.stereotype.Service;

/**
 * Questo servizio ci permette di avere una factory statica e non.
 * Questo se utilizzata nelle configurazioni, permetterà di generare bean
 * che verranno poi inseriti all'interno del container.
 *
 * La particolarità risiede nel fatto che PrintableFactory genera bean
 * da un interfaccia Concretamente non ne ha alcuna responsabilità.
 * I bean che verranno iniettati nel contesto da AppConf, non possiamo conoscere di che tipo essi siano
 * se non a Runtime, astraendoli completamente all'utilizzatore.
 */
@Service
public class PrintableFactory {
    public static Printable getPrintable(){
        //return new RoleFactory();
        return Shop.getInstance();
    }

    public Printable getPrintableNonStatic(){
        return new RoleFactory();
    }
}
