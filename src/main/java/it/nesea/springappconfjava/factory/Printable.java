package it.nesea.springappconfjava.factory;

public interface Printable {
    void print();
}
