package it.nesea.springappconfjava.factory;

import it.nesea.springappconfjava.beans.Role;
import it.nesea.springappconfjava.beans.User;

import java.util.Arrays;

/**
 * Questo ben è di natura printable ed a sua volta ha responsabilità su utente.
 * Questo esempio reca un problema, per quanto possa risultare funzionale:
 * Possono essere reati solamente utenti, e solamente amministratori.
 *
 */
public class RoleFactory implements Printable{
    public static User getAdmin(){
        Role adminRole = new Role("ADMIN", "ADMIN");
        return new User(1, "admin", "admin", Arrays.asList(adminRole));
    }

    public void print() {
        System.out.println("Hello from RoleFactory!");
    }
}
